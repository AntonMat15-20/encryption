import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Scanner;

/**
 * Класс для шифрования файлов TXT формата.
 */
public class Encrypt {

    public static void main(String[] args) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, IOException {

        File pathDirectory;
        String[] listDir;
        pathDirectory = new File(enter());
        listDir = pathDirectory.list();
        System.out.println(Arrays.toString(listDir));


        File[] files = pathDirectory.listFiles();
        assert files != null;
        for (File file : files) {
            if (file.getName().contains(".txt")) {
                encryption(file);
            }
            System.out.println("Программы завершила шифрование");
        }
    }

    /**
     * Метод для шифрования.
     */
    private static void encryption(File file) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, IOException {
        byte[] bytes;
        try (BufferedReader reader = new BufferedReader(new FileReader(file));
             BufferedWriter writer = new BufferedWriter(new FileWriter("Encryption.txt"))) {
            String line;
            SecretKeySpec KEY = new SecretKeySpec("fe1cbc02a3a697b0".getBytes("UTF-8"), "AES");
            while ((line = reader.readLine()) != null) {
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(Cipher.ENCRYPT_MODE, KEY);
                bytes = cipher.doFinal(line.getBytes());
                String result = Base64.getEncoder().encodeToString(bytes);
                writer.write(result);
                writer.flush();

            }
        } catch (IOException e) {
            System.err.println("ERROR");
            throw new IOException();
        }
    }

    /**
     * Метод для ввода пути.
     */
    private static String enter() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите путь: ");
        return scanner.nextLine();
    }

}