import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;


/**
 * Класс для дешифрования файлов TXT формата.
 */
public class Decrypt {


    public static void main(String[] args) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, IOException {

        String s;
        String line = "";
        try (BufferedReader reader = new BufferedReader(new FileReader("Encryption.txt"))) {
            while ((s = reader.readLine()) != null) {
                line += s;
            }
        } catch (IOException e) {
            System.err.println("ERROR");
        }

        byte[] decodedLine = Base64.getDecoder().decode(line);
        SecretKeySpec KEY = new SecretKeySpec("fe1cbc02a3a697b0".getBytes("UTF-8"), "AES");
        Cipher decryptCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        decryptCipher.init(Cipher.DECRYPT_MODE, KEY);
        byte[] decryptBytes = decryptCipher.doFinal(decodedLine);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter("Decryption.txt"))) {
            writer.write(new String(decryptBytes, "UTF-8"));
            writer.flush();
        }
        System.out.println("Дешифровка прошла успешно");
    }
}

